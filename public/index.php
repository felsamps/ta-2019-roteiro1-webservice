<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require '../vendor/autoload.php';

$app = new \Slim\App;

$app->get('/verifica-acesso', function (Request $request, Response $response, array $args) {
    //TODO completar dados com os ids das demais tags/cartões RFID
    $usersArray = array(
      "22 81 18 D3" => array("nome" => "Felipe", "permissao" => "Acesso autorizado"),
      "C6 E0 15 7E" => array("nome" => "Beltrano", "permissao" => "Acesso negado")
    );

    $uId = $request->getQueryParam("p");
    if($uId == "ALL") {
      $newResponse = $response->withJson($usersArray);
    }
    else {
      $userData = $usersArray[$uId];
      $newResponse = $response->withJson($userData);
    }

    return $newResponse;
});

$app->run();
